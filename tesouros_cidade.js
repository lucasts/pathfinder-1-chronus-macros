new Dialog
({
    title: 'City Size',
    content: `<form class="flexcol">
    <div class="form-group">
        <label>Select city size:</label>
        <select id="size">
            <option value="0" selected>Thorp</option>
            <option value="1">Hamlet</option>
            <option value="2">Village</option>
            <option value="3">Small Town</option>
            <option value="4">Large Town</option>
            <option value="5">Small City</option>
            <option value="6">Large City</option>
            <option value="7">Metropolis</option>      
        </select>
    </div>
</form>`,
    buttons:
	{
        Ok:
		{
            label: 'Ok',
            callback: async (html) =>
			{
                let level = Number($('#size :selected').val());
                if (level==0)
				{
					const message = await new Roll(`1d4`).toMessage();
					const roll = message.rolls[0];
					await ChatMessage.create({content: `Minor Items:`});
					for (var i = 0; i < roll.total;i++)
					{
						const result = await game.tables.getName('Generate Minor Magic Item').draw({displayChat: false});
						await ChatMessage.create({content: `${result.results.map(e => e.getChatText())}`});
					}
				}
				else if (level==1)
				{
					const message = await new Roll(`1d6`).toMessage();
					const roll = message.rolls[0];
					await ChatMessage.create({content: `Minor Items:`});
					for (var i = 0; i < roll.total;i++)
					{
						const result = await game.tables.getName('Generate Minor Magic Item').draw({displayChat: false});
						await ChatMessage.create({content: `${result.results.map(e => e.getChatText())}`});
					}
				}
				else if (level==2)
				{
					const message = await new Roll(`2d4`).toMessage();
					const roll = message.rolls[0];
					await ChatMessage.create({content: `Minor Items:`});
					for (var i = 0; i < roll.total;i++)
					{
						const result = await game.tables.getName('Generate Minor Magic Item').draw({displayChat: false});
						await ChatMessage.create({content: `${result.results.map(e => e.getChatText())}`});
					}
					const message2 = await new Roll(`1d4`).toMessage();
					const roll2 = message2.rolls[0];
					await ChatMessage.create({content: `Medium Items:`});
					for (var i = 0; i < roll2.total;i++)
					{
						const result = await game.tables.getName('Generate Medium Magic Item').draw({displayChat: false});
						await ChatMessage.create({content: `${result.results.map(e => e.getChatText())}`});
					}
				}
				else if (level==3)
				{
					const message = await new Roll(`3d4`).toMessage();
					const roll = message.rolls[0];
					await ChatMessage.create({content: `Minor Items:`});
					for (var i = 0; i < roll.total;i++)
					{
						const result = await game.tables.getName('Generate Minor Magic Item').draw({displayChat: false});
						await ChatMessage.create({content: `${result.results.map(e => e.getChatText())}`});
					}
					const message2 = await new Roll(`1d6`).toMessage();
					const roll2 = message2.rolls[0];
					await ChatMessage.create({content: `Medium Items:`});
					for (var i = 0; i < roll2.total;i++)
					{
						const result = await game.tables.getName('Generate Medium Magic Item').draw({displayChat: false});
						await ChatMessage.create({content: `${result.results.map(e => e.getChatText())}`});
					}
				}
				else if (level==4)
				{
					const message = await new Roll(`3d4`).toMessage();
					const roll = message.rolls[0];
					await ChatMessage.create({content: `Minor Items:`});
					for (var i = 0; i < roll.total;i++)
					{
						const result = await game.tables.getName('Generate Minor Magic Item').draw({displayChat: false});
						await ChatMessage.create({content: `${result.results.map(e => e.getChatText())}`});
					}
					const message2 = await new Roll(`2d4`).toMessage();
					const roll2 = message2.rolls[0];
					await ChatMessage.create({content: `Medium Items:`});
					for (var i = 0; i < roll2.total;i++)
					{
						const result = await game.tables.getName('Generate Medium Magic Item').draw({displayChat: false});
						await ChatMessage.create({content: `${result.results.map(e => e.getChatText())}`});
					}
					const message3 = await new Roll(`1d4`).toMessage();
					const roll3 = message3.rolls[0];
					await ChatMessage.create({content: `Major Items:`});
					for (var i = 0; i < roll3.total;i++)
					{
						const result = await game.tables.getName('Generate Major Magic Item').draw({displayChat: false});
						await ChatMessage.create({content: `${result.results.map(e => e.getChatText())}`});
					}
				}
				else if (level==5)
				{
					const message = await new Roll(`4d4`).toMessage();
					const roll = message.rolls[0];
					await ChatMessage.create({content: `Minor Items:`});
					for (var i = 0; i < roll.total;i++)
					{
						const result = await game.tables.getName('Generate Minor Magic Item').draw({displayChat: false});
						await ChatMessage.create({content: `${result.results.map(e => e.getChatText())}`});
					}
					const message2 = await new Roll(`3d4`).toMessage();
					const roll2 = message2.rolls[0];
					await ChatMessage.create({content: `Medium Items:`});
					for (var i = 0; i < roll2.total;i++)
					{
						const result = await game.tables.getName('Generate Medium Magic Item').draw({displayChat: false});
						await ChatMessage.create({content: `${result.results.map(e => e.getChatText())}`});
					}
					const message3 = await new Roll(`1d6`).toMessage();
					const roll3 = message3.rolls[0];
					await ChatMessage.create({content: `Major Items:`});
					for (var i = 0; i < roll3.total;i++)
					{
						const result = await game.tables.getName('Generate Major Magic Item').draw({displayChat: false});
						await ChatMessage.create({content: `${result.results.map(e => e.getChatText())}`});
					}
				}
				else if (level==6)
				{
					const message = await new Roll(`4d4`).toMessage();
					const roll = message.rolls[0];
					await ChatMessage.create({content: `Minor Items:`});
					for (var i = 0; i < roll.total;i++)
					{
						const result = await game.tables.getName('Generate Minor Magic Item').draw({displayChat: false});
						await ChatMessage.create({content: `${result.results.map(e => e.getChatText())}`});
					}
					const message2 = await new Roll(`3d4`).toMessage();
					const roll2 = message2.rolls[0];
					await ChatMessage.create({content: `Medium Items:`});
					for (var i = 0; i < roll2.total;i++)
					{
						const result = await game.tables.getName('Generate Medium Magic Item').draw({displayChat: false});
						await ChatMessage.create({content: `${result.results.map(e => e.getChatText())}`});
					}
					const message3 = await new Roll(`2d4`).toMessage();
					const roll3 = message3.rolls[0];
					await ChatMessage.create({content: `Major Items:`});
					for (var i = 0; i < roll3.total;i++)
					{
						const result = await game.tables.getName('Generate Major Magic Item').draw({displayChat: false});
						await ChatMessage.create({content: `${result.results.map(e => e.getChatText())}`});
					}
				}
				else if (level==7)
				{
					await ChatMessage.create({content: `Minor Items: All minor magic item is available`});
					const message2 = await new Roll(`4d4`).toMessage();
					const roll2 = message2.rolls[0];
					await ChatMessage.create({content: `Medium Items:`});
					for (var i = 0; i < roll2.total;i++)
					{
						const result = await game.tables.getName('Generate Medium Magic Item').draw({displayChat: false});
						await ChatMessage.create({content: `${result.results.map(e => e.getChatText())}`});
					}
					const message3 = await new Roll(`3d4`).toMessage();
					const roll3 = message3.rolls[0];
					await ChatMessage.create({content: `Major Items:`});
					for (var i = 0; i < roll3.total;i++)
					{
						const result = await game.tables.getName('Generate Major Magic Item').draw({displayChat: false});
						await ChatMessage.create({content: `${result.results.map(e => e.getChatText())}`});
					}
				}
            }
        },
        Cancel:
		{
            label: 'Cancel'
        }
    }
}).render(true);