const item = actor.items.getName("Mãos do Curandeiro");
await item.roll();
const {value, max} = item.system.uses;
if(!value)return ui.notifications.warn("Mãos do Curandeiro não tem mais uso.");
await item.update({"system.uses.value": value - 1});
ui.notifications.info("Mãos do Curandeiro tem mais "+value+" usos");


const message = await actor.rollSkill("hea", {skipDialog:true});
const roll = message.rolls[0];
const rollTotal = roll.total;
const skillInfo = actor.getSkillInfo("hea");
const ranks = skillInfo.rank;


if(rollTotal >= 20 && rollTotal < 25 && ranks >= 5 &&ranks < 10){
    ChatMessage.create({});{
    let rollData = '/h 2[Full Rest] * @attributes.hd.total[HD]'
    ui.chat.processMessage(rollData);
    }
}else if(rollTotal >= 25 && rollTotal < 30 && ranks >= 5 &&ranks < 10){
    ChatMessage.create({});{
        let rollData = '/h 2[Full Rest] * @attributes.hd.total[HD] + @abilities.wis.mod[Wisdom Modifier]'
        ui.chat.processMessage(rollData);
    }
}else if(rollTotal >=30 && ranks>=5 &&ranks<10){
    ChatMessage.create({});{
        let rollData = '/h 2 * @attributes.hd.total[HD] + @abilities.wis.mod[Wisdom Modifier] + @skills.kpl.rank[Planes Rank]'
        ui.chat.processMessage(rollData);
    }
}