let target = game.user.targets.first().actor;
let race = target.items.find( o=> o.type === 'race');
const rollMode = game.settings.get("core", "rollMode");
const creatureType = race.system.creatureType;

const knowledges = {
  aberration: "kdu",
  animal: "kna",
  construct: "kar",
  dragon: "kar",
  fey: "kna",
  humanoid: "klo",
  magicalBeast: "kar",
  monstrousHumanoid: "kna",
  ooze: "kdu",
  outsider: "kpl",
  plant: "kna",
  undead: "kre",
  vermin: "kna",
};

if(actor.getSkillInfo(knowledges[creatureType]).rank > 0)
{
    game.settings.set("core", "rollMode", "blindroll");
    const message = await actor.rollSkill(knowledges[creatureType], {skipDialog:true});
    game.settings.set("core", "rollMode", rollMode);
}
else
{
    return;
}